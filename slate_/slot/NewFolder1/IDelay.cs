﻿namespace nilnul.task
{
	/// <summary>
	/// the task's start is delayed after the specified start time;
	/// </summary>
	public interface IDelay { }
}
