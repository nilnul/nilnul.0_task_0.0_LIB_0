namespace nilnul.task
{
	/// <summary>
	/// how a long a task takes.
	/// </summary>
	/// <remarks>
	/// note: the duration is not a distribution, but a fixed|definitude value;
	/// for indefinite duration, <see cref="task.IDurant"/>
	/// </remarks>
	public interface IDuration { }

}
