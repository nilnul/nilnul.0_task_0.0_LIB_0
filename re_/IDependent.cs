﻿namespace nilnul.task.re_
{
	/// <summary>
	/// eg:
	///		a reminder is an affiliate. and if the main task is removed, the reminder would be removed.
	/// </summary>
	/// alias:
	///		
	///		derived
	///		dependent
	interface IAffiliate { }
}
