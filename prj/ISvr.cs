﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task_.prj
{
	/// <summary>
	/// <see cref="nilnul.task.ISvr"/> doesnot support <see cref="IProject"/> in that:
	///		1) no <see cref="task.co_.Prior"/>
	///		2) <see cref="task.slate_"/> is literal valued, not dynamically valued.
	///	but we can extend <see cref="task.svr_.Google"/> by customizing the description of a task in <see cref="nilnul._xml._el.IContent"/> format:
	///		1) <see cref="task.co_.Prior"/> is represented;
	///		2) <see cref="task._slate.slot_._based.Duration"/>
	/// </summary>
	internal class ISvr:nilnul.task.ISvr
	{
	}
}
