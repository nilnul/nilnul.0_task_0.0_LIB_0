using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task.slates
{
	/// <summary>
	/// in google, this is called calendar.
	/// </summary>
	/// <remarks>
	/// grouping the slates;
	/// this can also embody:
	///		1) a time scale: day, hour, week,month,year, minute, second.
	///		2) free|busy|holday|workday|officeTime|weekend, marked; that is some period tagged.
	/// </remarks>
	/// alias:
	///		chronicle
	///			,chron
	///			,chronic
	///			, a cluster of slates;
	///		planner
	///			,a planner for a project
	///		agenda
	///		
	///		Choronology
	///		choreography
	///		organizer
	///		organ
	///		orgy
	///		org
	///		journal
	///		ledger
	///		arrange
	///		diary
	///		scheduler
	///		yearbook
	///		timeline
	///	<see cref="nilnul.task.svr_.google.usr.Chronology"/>	
	public interface IChronicle
	{
	}
}
