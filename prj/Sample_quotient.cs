using nilnul.num.rational;
using nilnul.task.co_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rational = nilnul.num.rational.Rational_InheritFraction;

namespace nilnul.task.prj
{
	/// <summary>
	/// proj is executed in simulation, such that the duration of task is a definite value
	/// </summary>
	///
	public class Sample_quotient
	{
		public Prj project;

		public TaskDuration[] durations;

		public Rational duration(Duration task) {
			return durations.Where(c => c.task == task).First().duration;
		}

		public IEnumerable<Duration> tasks {
			get {
				return project.precedences.tasks;
			}
		}


		/// <summary>
		/// the state is:
		/// durant sampled. 
		/// </summary>
		public class TaskDuration {
			public Duration task;

			public Rational duration;

			public void sample() {

				var t= //nilnul.num.quotient.of_.unary_.
					nilnul.num.quotient_.radix.ext.errable_.ieee_.bin.to_._ToRationalX.ToSignificed(
					//Rational_InheritFraction.ToRational_tillDenominator(
					task.distribution.sample()
					//,10000
				).toDenomNonnil();

				duration = new Rational(
					t.numerator,t.denominator
				);

			}

			static public TaskDuration Sample(Duration task) {
				var r = new TaskDuration();
				r.task = task;
				r.sample();
				return r;
			}

	


		}


		static public Sample_quotient Realize(Prj proj) {

			var r = new Sample_quotient();
			r.project = proj;
			r.durations = proj.precedences.tasks.Select(c => TaskDuration.Sample(c)).ToArray();
			return r;
			
		}

		/// <summary>
		/// samples
		/// </summary>
		/// <param name="proj"></param>
		/// <param name="times"></param>
		/// <returns></returns>
		static public IEnumerable< Sample_quotient> Realize(Prj proj, int times)
		{

			return System.Linq.Enumerable.Range(0, times).Select(c => Realize(proj));

		}
	}
}
