namespace nilnul.task.co_.prior_
{
	/// <summary>
	/// a period of time is needed between the two tasks;
	/// </summary>
	/// <remarks>
	/// a <see cref="deed_.empty.ISlot "/> as wait is inserted. Now the duo will become two duos:
	///		the former, the wait
	///		: the wait, the latter.
	/// </remarks>
	internal class IBuffer
	{
	}



}
