﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task._attrs
{
	internal enum Li_
	{
		/// <summary>
		/// time
		/// </summary>
		Schedule
			,
		/// <summary>
		/// money
		/// </summary>
		Finance
			,
		/// <summary>
		/// contact, stakeholders
		/// </summary>
		Participant
			,
		/// <summary>
		/// the objective, target, aim; what to do; anticipation;
		/// </summary>
		Target
	}
}
