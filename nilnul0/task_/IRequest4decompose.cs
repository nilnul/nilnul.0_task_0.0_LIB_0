﻿namespace nilnul.task_
{
	/// <summary>
	/// request for decomposition;
	/// </summary>
	/// <remarks>
	/// even requested for decomposition due to say that it's too big, the task might be still nondecomposable.
	/// </remarks>
	/// alias:
	///		complex,
	///		, vs:<see cref="Composite"/>
	public interface IRequest4decompose:ITask { }
}
