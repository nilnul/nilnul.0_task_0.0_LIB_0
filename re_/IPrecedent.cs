﻿namespace nilnul.task.re_
{
	/// <summary>
	/// the task must be done before the other task is finished;
	/// </summary>
	interface IPrecedent { }
}
