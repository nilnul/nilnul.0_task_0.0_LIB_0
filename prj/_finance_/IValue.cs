﻿namespace nilnul.task_.prj._finance_
{
	/// <summary>
	/// quotation;price;
	/// </summary>
	/// <remarks>
	/// we can use internal return rate, etc, to compute the value|investmentReturnRate of the prj|task; for a prj, there is a sequence of tasks, each of which brings some cost or revenue at some time point;
	/// </remarks>
	interface IValue { }
}
