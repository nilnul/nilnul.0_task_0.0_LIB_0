﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task_.xpn_
{
	/// <summary>
	/// eg:
	///		in visual studio, we might note "todo:"; but later we find out that approach is not right and need to disregard.
	///	to know which path is not treadable would save us a lot of time;
	/// </summary>
	/// alias:
	///		toNotDo
	///		to0do
	///		
	internal class IToNotDo
	{
	}
}
