using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task._prj_._prior
{
	/// if the duration is definite, then the task is called deed.
	/// 
	/// <summary>
	/// the duration conforms to a distribution, not a definite real number.
	/// </summary>
	/// alias:
	///		task
	///		todo
	///		prospect
	///		perf
	///		performant
	///		performing
	///		resp
	///		responsibility
	///		effort
	///		planned item
	///		
	///		backlog
	///		
	/// 
	internal class ITodo:task.duration.IDistribution
	{
	}
}
