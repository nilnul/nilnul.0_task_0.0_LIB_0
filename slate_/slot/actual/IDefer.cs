﻿namespace nilnul.task
{
	/// <summary>
	/// the task's start is delayed after the specified start time;
	/// </summary>
	/// <remarks>
	/// means the following task is prepended some timespan;
	/// </remarks>
	public interface IDefer { }
}
