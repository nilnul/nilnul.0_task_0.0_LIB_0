﻿namespace nilnul.task
{
	/// <summary>
	/// the slack/float after the end of the task, before deadline;
	/// </summary>
	public interface IDoodle { }
}
