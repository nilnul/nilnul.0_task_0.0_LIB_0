﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task._remind.alarm
{
	
	/// <remarks>
	/// eg:repeats 4 additional times with a 5-minute delay after the      initial triggering of the alarm:
	///		REPEAT:4
	///		DURATION:PT5M
	///			,where PT means period of time?
	/// </remarks>
	internal interface IRepeat
	{
	}
}
