using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task.duration
{
	/// <summary>
	/// the duration conforms to a <see cref="nilnul.stat.IDist"/>;
	/// this can generate <see cref="task.IDuration"/> by sampling|realization|reification
	/// </summary>
	/// <see cref="task.IDurant"/>
	/// 
	/// alias:
	///		IStatistic
	internal class IDistribution
	{
	}
}
