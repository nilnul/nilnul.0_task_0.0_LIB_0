namespace nilnul.deed_
{
	/// <summary>
	/// the deed is recorded, in, say, yearbook; then we can analyze it and the knowledge and experience would help us in doing future deeds.
	/// to help future deeds using the knowledge, we need to plan, not on imprompto.
	/// </summary>
	interface IRecorded { }

}
