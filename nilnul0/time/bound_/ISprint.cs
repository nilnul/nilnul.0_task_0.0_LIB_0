using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.time.bound_
{
	/// <summary>
	/// a sprint, or a period of time to run some scrum for one backlog.
	/// </summary>
	/// <remarks>
	/// for <see cref="nilnul.task.slates.IChronicle"/>, we may add some sprints or additional timelines to the canonical timeline;
	/// </remarks>
	/// alias:
	///		activity
	///		sprint
	///		timeline
	internal class ISprint
	{
	}
}
