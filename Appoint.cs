﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task
{
	/// <summary>
	/// plan a task with resources like: time and venue and participants.
	/// </summary>
	/// <remarks>
	/// alias:
	///		plan
	///			same starter "p" as in "proj"
	///		plot
	///		ploy
	///			like in deploy
	///		provision
	///		preparatin
	///		
	///		occur
	///		occasion
	///		schedule
	///		assign
	///		appoint
	///			determine or decide on (a time or a place).
	///			assign a job or role to (someone)
	///		allot
	///			resources: time, venue, humans
	///		allocate
	///		tack
	///			like tack it to the chart/schedule
	/// </remarks>
	public interface IAppoint
	{
	}
}
