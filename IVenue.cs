namespace nilnul.task
{
	/// <summary>
	/// location for the task to happen;
	/// </summary>
	/// alias:
	///		space
	///		venue
	///		location, used for fs;
	interface IVenue :ISubstance{ }
}
