namespace nilnul.task.co_
{
	/// <summary>
	/// the task must be done before the other task is started;
	/// </summary>
    /// alias:
    ///		priority
    ///		precedent
    ///		dependent
	///		then
    ///	vs:
    ///	    subsequent
	public interface IPrior { }
}
