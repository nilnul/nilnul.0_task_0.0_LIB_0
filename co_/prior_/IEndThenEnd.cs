namespace nilnul.task.co_.prior_
{
	/// <summary>
	/// for <see cref="task.co_.IPrior"/>:
	///		former ends, then latter can end
	///	;
	///	
	/// </summary>
	/// <remarks>
	///  to <see cref="prior.of_._CanonizeX"/>:
	/// the latter splits as the main, and the nominal end.
	///  thence,
	///		: the main and the nominal end will be <see cref="co_.prior_.IEndThenStart"/>
	///		: the former and the nominal end will be <see cref="co_.prior_.IEndThenStart"/>
	/// </remarks>
	internal class IEndThenEnd
	{
	}



}
