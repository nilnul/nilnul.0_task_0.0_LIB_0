﻿namespace nilnul.task
{
	/// <summary>
	/// if the <see cref="IStream"/> is stopped before some time;
	/// </summary>
	interface IStr { }
}
