﻿namespace nilnul.task
{
	/// <summary>
	/// time, money, human, substance, data(info/doc) to support the task;
	/// </summary>
	/// <remarks>
	/// to manage a task, we need to manage its resources, as resources are constained.
	/// </remarks>
	/// allot
	interface IResource { }
}
