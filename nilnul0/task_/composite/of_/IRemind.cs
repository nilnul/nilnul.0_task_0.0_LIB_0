﻿namespace nilnul.task_.composite.of_
{
	/// <summary>
	/// a task is split into two: the reminder in advance, the main activity then taken;
	/// </summary>
	/// <see cref="task_.IReminder"/>
	/// 
	interface IRemind { }

}
