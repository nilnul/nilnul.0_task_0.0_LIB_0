using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// used in history to describe an event for a consecutive or a sequence of time and location.
	/// </remarks>
	/// alias:
	///		task
	///		deed
	///			,ded?
	///		todo
	/// vs:
	///		task which means todo, deed can also mean sth done.
	///			,did, done, doing,todo
	///		
	///			
	internal class IDeed
	{
	}
}
