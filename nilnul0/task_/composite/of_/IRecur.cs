﻿namespace nilnul.task_.composite.of_
{
	/// <summary>
	/// a task with recurring periods;
	/// </summary>
	/// <seealso cref="nilnul.task.slate_.ISlots"/>, where the slots are disjoint and sporadic;
	interface IRecur { }

}
