﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task.slate_
{
	/// <summary>
	/// according some management rule: if a task takes less than n minutes to complete, do it right away, where n =2
	/// </summary>
	/// alias:
	///		quick
	///		
	static public class _RightAwayX
	{
		public const int MINUTES = 2;
	}
}
