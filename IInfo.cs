﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task
{
	/// <summary>
	///  properties, attributes, constraints, relations, context;
	/// such as duration, intended begin & end time.
	/// </summary>
	/// <remarks>
	///  this can be a tip in how to schedule the task.
	///  eg:
	///		a task might be scheduled no later than a certain date;
	/// </remarks>
    /// alias:
    ///		meta
	internal class IInfo
	{
	}
}
