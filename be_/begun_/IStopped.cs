﻿namespace nilnul.task.be_.begun_
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// in google iCalendar, this is serialized as vJournal;
	/// </remarks>
	/// alias:
	///		stop
	///		ceased
	///		shut
	///		halt
	///		terminate
	///		quit
	///		end
	///		unattended
	///		abandoned
	///		discontinue
	///		stay
	///		belay
	///		left
	public interface IStopped :IBegun
	{
		
	}

}
