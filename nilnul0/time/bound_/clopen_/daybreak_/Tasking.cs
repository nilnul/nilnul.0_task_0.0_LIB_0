using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.time.bound_.clopen_.daybreak_
{
	/// like fiscal year is not aligned with natural year, tasking day is not harmonic with natural day.
	/// <summary>
	/// for each day, we settle|rest at dusk, and rise at dawk
	/// </summary>
	/// 日出而作，日落而息；
	/// alias:
	///		daybreak
	///		作息
	///		routine
	///		settle
	///	
	static public class _TaskingX
	{
		/// <summary>
		/// the time after supper. One shall back into one's den.
		/// </summary>
		/// <remarks>
		/// after this time, it's recluse time for one to reflect.
		/// </remarks>
		/// alias:
		///		recluse
		///		settle
		///		reflect
		///		reset
		///		rest
		///		
		/// or:
		///		19:30?
		///		21:00?
		static public readonly TimeSpan SETTLE= new TimeSpan(18,0,0);





	}
}
