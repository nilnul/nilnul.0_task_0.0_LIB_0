﻿namespace nilnul.task
{
	/// <summary>
	/// <see cref="nilnul.thing"/>s, materials including equipments used for a task;
	/// </summary>
	/// <remarks>
	/// physical needs;
	/// </remarks>
	/// alias:
	///		material
	///		thing <see cref="task._resources.Li_.Thing"/>
	///		
	interface ISubstance { }
}
