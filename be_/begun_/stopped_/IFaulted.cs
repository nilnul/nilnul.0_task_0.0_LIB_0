﻿namespace nilnul.task.be_.begun_
{
	/// <summary>
	/// the task is aborted due to some error (including cancellation). it's not successful, which is observable at the objective it's working on;
	/// </summary>
	/// alias:
	///		faulty
	///		faulted
	public interface IFaulty:IBegun { }

}
