﻿namespace nilnul.task
{
	/// <summary>
	/// the timespan used by the task, ie, from start to end;
	/// </summary>
	public interface IDuring { }
}
