﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task.rel_.prec.node.be_
{
	/// <summary>
	/// for it to be a project root, it has to be:
	///		- independent. that is it has no precedent tasks
	///		- nontrivia. that is it has one or more subsequent tasks.
	///	the project would include the root, and all its subsequent tasks;
	/// </summary>
	internal class IProject
	{
	}
}
