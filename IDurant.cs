using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task
{
	/// <summary>
	/// the duration conforms to a statistic distribution
	/// </summary>
	/// alias:
	///		durare
	///			,the latin
	///		tenure
	///		longevity
	///		lifespan
	///		endure
	///		endure
	///		dure
	///		dural
	///		durate
	///		distribution
	///		distr
	///		
	public interface IDurant
	{
	}
}
