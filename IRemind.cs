﻿namespace nilnul.task
{
	/// <summary>
	/// associate a collection of alarms to a task.
	/// </summary>
	/// alias:
	///		alert
	///		alarms
	///		reminders
	interface IRemind { }
}
