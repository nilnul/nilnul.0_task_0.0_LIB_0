﻿namespace nilnul.task._slate
{
	/// <summary>
	/// the latest end of a task
	/// </summary>
	/// <remarks>
	/// duration might be lt maxEnd-minStart;
	/// </remarks>
	interface IMaxEnd { }
}
