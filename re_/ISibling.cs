﻿namespace nilnul.task.re_
{
	/// <summary>
	/// for grouping,  we can make Task2 a sibling to Task1, thus we don't need to create a third master task;
	/// </summary>
	/// <remarks>
	/// a way to create <see cref="nilnul.tasks"/>
	/// </remarks>
	interface ISibling { }
}
