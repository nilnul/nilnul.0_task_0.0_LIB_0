using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task_
{
	static public class _MaskX
	{
		public const string Name = "mask";
	}
	/// <summary>
	/// time slot scheduled for an action
	/// from the earlier start to latest finish.
	/// </summary>
	/// <remarks>
	/// slack allowed for actual action;
	/// </remarks>
	/// alias:
	///		master
	///		mast
	///			,like the mast in a boat as an emblem
	///		mask
	///			,like task
	///		plan
	///		budget
	///	vs:
	///		execution
	///		action
	///		act
	///
	public interface IMask { }
}
