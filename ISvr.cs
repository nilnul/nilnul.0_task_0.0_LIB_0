﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task
{
	/// <summary>
	/// this is a svr for task, including slating a task into one of many organizers;
	/// </summary>
	/// <remarks>
	/// this is not, but based on, time server;
	/// for <see cref="nilnul.task_.IProject"/>, <see cref="nilnul.task.co_.Prior"/> exists among subtasks;
	/// <see cref="task.svr_.Google"/> doesnot support the <see cref="nilnul.task.co_.Prior"/> or un<see cref="nilnul.task.slate_"/>.
	/// </remarks>
	internal class ISvr
	{
	}
}
