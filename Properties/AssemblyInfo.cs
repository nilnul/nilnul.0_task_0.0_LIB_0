using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

/*
 application:
	note,
		, a quick event, which is trivia to create, is a note;
	diary,
		,a event created in the past, or overdue is taken as a diary
	todo|task without being schduled|slated,
		, a event|slate that is special by setting some properties, if not supported by a schuduler softare.
		,a task scheduled in the future. even those that are ongoing (part is in the past, part is in the future)
	schedule,
		,scheduled task
	,
	project,
		,eg, a calendar|chronicle, can hold a project, in which tasks are correlately scheduled, and resources management is added.
 */

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("nilnul._task_._LIB_")]
[assembly: AssemblyDescription("task; scheduling of tasks into a project|prj. Each task can be simulated using "+ nameof(System.Threading.Tasks.Task) + ";")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("nilnul.com")]
[assembly: AssemblyProduct("nilnul._task_._LIB_")]
[assembly: AssemblyCopyright("wangyoutian@nilnul.com")]
[assembly: AssemblyTrademark("nilnul._task_._LIB_")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c4515343-73d7-431a-b0d6-9ce9e1d3ab83")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1")]
[assembly: AssemblyFileVersion("1.0.0.0")]
