namespace nilnul.task.co_.prior_
{
	/// <summary>
	/// for <see cref="task.co_.IPrior"/>:
	///		former starts, then latter can end
	///	;
	///	
	/// </summary>
	/// <remarks>
	///  to <see cref="prior.of_._CanonizeX"/>:
	/// the former splits as
	///		:the nominal start and the main
	///  thence,
	///		: the nominal start and the main will be <see cref="co_.prior_.IEndThenStart"/>
	///		: the nominal start and the latter will be <see cref="co_.prior_.IEndThenEnd"/>
	///			,which can then further canonize as <see cref="IEndThenEnd"/>
	/// </remarks>
	internal class IStartAndEnd
	{
	}



}
