using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.deed_.empty
{
	/// <summary>
	/// <see cref="nilnul.task_.IEmpty"/> slotted to a duration.
	/// </summary>
	/// alias:
	///		mask
	///		doodle
	///		fare
	///		wait
	internal class ISlot
		:nilnul.task.ISlot
	{
	}
}
