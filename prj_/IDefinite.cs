namespace nilnul.task.prj_
{
	/// <summary>
	/// not <see cref="prj_.IPensive"/>
	/// </summary>
	/// <see cref="prj.ISample"/>
	/// alias:
	///		actual
	///			,act
	///		reify
	///		definite
	///		unpensive
	///		unrandom
	///		realized
	public interface IDefinite:IPrj
	{
	}


}
