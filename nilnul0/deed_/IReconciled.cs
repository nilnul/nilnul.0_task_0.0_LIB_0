namespace nilnul.deed_
{
	/// <summary>
	/// there is a plan. And the actual execution's progress is used to update the plan.
	/// 
	/// </summary>
	interface IReconciled :IPlanned, IRecorded{ }

}
