using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.task.prj_
{
	/// <summary>
	/// the duration of each task follows a distribution.
	/// </summary>
	/// <remarks>
	///	this is the default implementation of prj;
	///
	/// </remarks>
	/// alias:
	///		predictive
	///		contemplative
	///		meditative
	///		pensive
	///		pendant
	///		pending
	///			,might connotate a prj shelved.
	///		speculative
	///		
	public interface IPensive:IPrj
	{
	}


}
