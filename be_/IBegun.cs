﻿namespace nilnul.task.be_
{
	/// <summary>
	/// the task has been started;
	/// it might be finished or not.
	/// </summary>
	/// alias:
	///		started
	///		begun
	public interface IBegun { }
}
